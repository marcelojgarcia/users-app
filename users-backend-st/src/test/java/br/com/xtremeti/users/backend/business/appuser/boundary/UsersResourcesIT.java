package br.com.xtremeti.users.backend.business.appuser.boundary;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.core.IsNull.nullValue;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

public class UsersResourcesIT {

    private Client client;
    private WebTarget target;

    @Before
    public void before() {
        client = ClientBuilder.newClient();
        target = client.target(System.getProperty("targetServer") + "/users");
    }

    @After
    public void after() {
        client.close();
    }

    @Test
    public void testListAllUsers() {

        Response response = target.request(MediaType.APPLICATION_JSON).get();

        String result = response.readEntity(String.class);

        assertThat(response.getStatus(), is(200));
        assertThat(result, not(nullValue()));

        try (JsonReader reader = Json.createReader(new StringReader(result))) {

            JsonArray users = reader.readArray();
            assertThat(users.size(), greaterThanOrEqualTo(1));
        }
    }

    @Test
    public void testGetUserById() {

        String json = "{\"username\":\"user2get\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"2019-01-01\","
                + "\"profile\":\"administrador\""
                + "}";

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        JsonObject created = getFromResponse(response);

        Response getResponse = target
                .path(created.getInt("id") + "")
                .request(MediaType.APPLICATION_JSON)
                .get();

        JsonObject registered = getFromResponse(getResponse);

        assertThat(registered.getInt("id"), is(created.getInt("id")));
    }

    @Test
    public void testCreateUser() {

        String json = "{\"username\":\"testCriarUsuario\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"2019-01-01\","
                + "\"profile\":\"administrador\""
                + "}";

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        JsonObject appuser = getFromResponse(response);
        assertThat(appuser.getInt("id"), greaterThan(0));
    }

    @Test
    public void testInvalidBirthDate() {

        String json = "{\"username\":\"testCriarUsuario\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"fdsafadsf\","
                + "\"profile\":\"administrador\""
                + "}";

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        String result = response.readEntity(String.class);

        assertThat(response.getStatus(), is(422));
        assertThat(result, not(nullValue()));

        try (JsonReader reader = Json.createReader(new StringReader(result))) {

            JsonObject error = reader.readObject();
            assertThat(error.getString("error"), not(nullValue()));
        }

    }

    @Test
    public void testInvalidJSON() {

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity("{}", MediaType.APPLICATION_JSON));

        checkError(response, 400, "Invalid JSON");
    }

    @Test
    public void testEmptyJSON() {

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity("", MediaType.APPLICATION_JSON));

        checkError(response, 400, "Invalid JSON");
    }

    @Test
    public void testUsernameTaken() {

        String json = "{\"username\":\"someuser2\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"2019-01-01\","
                + "\"profile\":\"administrador\""
                + "}";

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        JsonObject created = getFromResponse(response);

        Response secondTry = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        checkError(secondTry, 400, "The username is already taken");
    }

    @Test
    public void testUpdateUser() {

        String json = "{\"username\":\"user2update\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"2019-01-01\","
                + "\"profile\":\"administrador\""
                + "}";

        Response response = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));

        JsonObject created = getFromResponse(response);

        Response updateResponse = target
                .path(created.getInt("id") + "")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(json.replace("Test User", "Update User"), MediaType.APPLICATION_JSON));

        JsonObject updated = getFromResponse(updateResponse);

        assertThat(updated.getInt("id"), is(created.getInt("id")));
        assertThat(updated.getString("name"), not(created.getString("name")));
        assertThat(updated.getString("name"), is("Update User"));
    }

    private JsonObject getFromResponse(Response response) {

        assertThat(response.getStatus(), is(200));

        String result = response.readEntity(String.class);

        assertThat(result, not(nullValue()));

        try (JsonReader reader = Json.createReader(new StringReader(result))) {

            return reader.readObject();

        }
    }

    private void checkError(Response response, int status, String errorMessage) {

        assertThat(response.getStatus(), is(status));

        String result = response.readEntity(String.class);

        assertThat(result, not(nullValue()));

        try (JsonReader reader = Json.createReader(new StringReader(result))) {

            JsonObject error = reader.readObject();
            assertThat(error.getString("error"), is(errorMessage));
        }
    }

}
