package br.com.xtremeti.users.backend.business.session.boundary;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.core.IsNull.nullValue;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

public class SessionsResourcesIT {

    private Client client;
    private WebTarget target;

    @Before
    public void before() {
        client = ClientBuilder.newClient();

        target = client.target(System.getProperty("targetServer") + "/sessions");
    }

    @After
    public void after() {
        client.close();
    }

    @Test
    public void testListarSessoes() {

        Response response = target.request(MediaType.APPLICATION_JSON).get();

        String result = response.readEntity(String.class);

        assertThat(response.getStatus(), is(200));
        assertThat(result, not(nullValue()));

        try (JsonReader reader = Json.createReader(new StringReader(result))) {
            
            JsonArray sessions = reader.readArray();
            assertThat(sessions.size(), greaterThanOrEqualTo(1));
            
        }
    }

}
