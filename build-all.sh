#!/bin/sh
./create-docker-network.sh
./database/build.sh
./users-backend/build-base-server.sh
./users-backend/build-docker.sh
./users-frontend/build-prod.sh
./users-frontend/build-docker.sh
