var LoginPage = function(){

    this.username = element(by.id('username'));
    this.password = element(by.id('userpass'));
    this.loginButton = element(by.id('login'));
    this.message = element(by.id('message'));

    this.navigateToPage = function(){

        browser.waitForAngularEnabled(false);       
        browser.get('http://localhost:3000/login.html');

    }

    this.doLogin = function(username,password){

        this.username.sendKeys(username);
        this.password.sendKeys(password);
        this.loginButton.click();
    }

}

module.exports = LoginPage;