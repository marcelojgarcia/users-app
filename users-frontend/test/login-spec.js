var LoginPage = require('./login-page');
var ElementUtils = require('./element-utils');

describe('Página de autenticação', function() {
    
    var loginPage = new LoginPage();    
    var elementUtils = new ElementUtils();

    beforeEach(function() {
        
        loginPage.navigateToPage();        

      });

    it('deve ser solicitar username e password', function() {

        expect(loginPage.username.isPresent()).toBe(true);
        expect(loginPage.password.isPresent()).toBe(true);

    });

    it('deve informar falha na autenticação', function() {

        loginPage.doLogin('invalido','invalido');

        expect(elementUtils.hasClass(loginPage.message,'hidden')).toBe(false);

    });

    it('deve direcionar para lista de usuários apoś autenticação com sucesso', function(){

        loginPage.doLogin('admin','admin');

        expect(browser.getCurrentUrl()).toContain('main.html');

    });

  });