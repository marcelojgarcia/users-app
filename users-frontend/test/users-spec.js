var UsersPage = require('./users-page');

/*
Depende do login executado corretamente em login-spec.js
Isso não é considerado boa prática, pois as especificações e os testes devem ser independentes entre si
Deve ser revisto
*/
describe('Lista de usuários', function () {

    var usersPage = new UsersPage();

    beforeEach(function () {

        usersPage.navigateToPage();

    });

    it('deve apresentar todos os registrados', function () {

        expect(usersPage.usersList.count()).toBeGreaterThan(0);

    });

    it('deve permitir selecionar usuário para alterar', function () {

        usersPage.editUser();

        expect(browser.getCurrentUrl()).toMatch(new RegExp('.*/#/users/\\w+'));

    });

});