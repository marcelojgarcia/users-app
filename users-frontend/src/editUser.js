import { html, render } from 'lit-html';
import { UsersService } from './usersService.js';
import DatePickerI18n from './dtpickerI18n.js';

class EditUser extends HTMLElement {

    constructor() {
        super();
        console.log('Constructor UsersForm');
        this.root = this.attachShadow({ mode: 'open' });
        this.userService = new UsersService();
        this.dtpickerI18n = new DatePickerI18n();
    }

    get userid() {
        return this.getAttribute('data-userid');
    }

    connectedCallback() {
        console.log('ConnectedCallback UsersForm for userid: ', this.userid);
        render(this.template(), this.root);
        this.dtpickerI18n.addI18n(this.root.querySelector('vaadin-date-picker'));
        this.root.querySelector('vaadin-combo-box').items = ['Usuário', 'Admin', 'Convidado'];
        this.loadUser();
    }

    template() {

        const updateUser = {
            handleEvent: (e) => this.handleUpdate(e),
            capture: true
        }

        return html`
        <style>
            :host{
                display: block;
                margin: 10px auto;
                padding: 0.5em;
                max-width: 700px;        
            }
            vaadin-button {
                background-color: var(--button-bk-color);
                color: var(--button-color);
            }
            .form {
                position: relative;
                z-index: 1;
                background: #FFFFFF;
                max-width: 460px;
                margin: 0 auto 0px;
                padding: 45px;
                text-align: center;
                box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
            }
            .form .message {
                margin: 15px 0 0;
                color: red;
                font-size: 16px;
            }                  
        </style>
        <div class="form">
            <vaadin-form-layout>
                <vaadin-text-field id="username" label="Username" @readonly=${this.userId === 0} 
                required minlength="8" maxlength="10" error-message="Tamanho mínimo 8 e máximo 10"></vaadin-text-field>
                <vaadin-text-field id="name" label="Nome" 
                required minlength="2" maxlength="10" error-message="Tamanho mínimo 2 e máximo 10"></vaadin-text-field>
                <vaadin-text-field colspan="2" id="email" label="E-mail"
                pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required error-message="informe e-mail válido"></vaadin-text-field>
                <vaadin-date-picker id="dtNascimento" label="Data Nascimento" placeholder="Escolha a data"
                required error-message="preenchimento obrigatório">
                </vaadin-date-picker>
                <vaadin-combo-box id="privilegios" label="Privilégios"></vaadin-combo-box>
                <vaadin-button type="submit" @click=${updateUser}>Confirmar</vaadin-button>
            </vaadin-form-layout>
            <vaadin-notification id="notification" duration="4000"></vaadin-notification>
        </div>`;
    }

    notificationRenderer(root, result) {

        // Check if there is a content generated with the previous renderer call not to recreate it.
        if (root.firstElementChild) {
            return;
        }

        const container = window.document.createElement('div');
        const boldText = window.document.createElement('b');
        boldText.textContent = 'Notice';

        const br = window.document.createElement('br');
        let plainText

        //If success
        if (!result.error) {
            plainText = window.document.createTextNode('The user ' + result.username + ' has been created/edited!');
        } else {

            //If failure
            plainText = window.document.createTextNode(result.error);
        }

        container.appendChild(boldText);
        container.appendChild(br);
        container.appendChild(plainText);

        root.appendChild(container);
    }

    showNotification(result) {
        const notification = this.root.getElementById('notification');
        notification.renderer = root => this.notificationRenderer(root, result);
        notification.open();
    }

    handleUpdate(e) {
        const username = this.root.getElementById('username');
        const name = this.root.getElementById('name');
        const email = this.root.getElementById('email');
        const dtNascimento = this.root.getElementById('dtNascimento');
        const privilegios = this.root.getElementById('privilegios');

        username.validate();
        name.validate();
        email.validate();
        dtNascimento.validate();
        privilegios.validate();

        if (username.invalid ||
            name.invalid ||
            email.invalid ||
            dtNascimento.invalid ||
            privilegios.invalid) {
            return;
        }

        const user = {
            id: this.userid,
            username: username.value,
            name: name.value,
            email: email.value,
            birthDate: dtNascimento.value,
            profile: privilegios.value
        };
        if (user.id === '0') {
            this.userService.create(user)
                .then(result => this.showNotification(result))
                .catch(error => {
                    console.log(error);
                });
        } else {
            this.userService.update(user)
                .then(result => this.showNotification(result))
                .catch(error => {
                    console.log(error);
                });
        }
    }

    loadUser() {

        this.userService.getUser(this.userid)
            .then(user => {
                this.root.getElementById('username').setAttribute('readonly', true);
                this.root.getElementById('username').setAttribute('value', user.username);
                this.root.getElementById('name').setAttribute('value', user.name);
                this.root.getElementById('email').setAttribute('value', user.email);
                this.root.getElementById('dtNascimento').setAttribute('value', user.birthDate);

                let combo = this.root.querySelector('vaadin-combo-box');
                combo.value = user.profile;
            })
            .catch(error => console.log(error));
    }
}

customElements.define('edit-user', EditUser);