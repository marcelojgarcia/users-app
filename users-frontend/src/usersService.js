export class UsersService {

    getUsers() {
        return fetch('USERS_URL')
            .then(response => response.json());
    }

    update(user) {
        return fetch('USERS_URL/' + user.id, {
            method: "PUT",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(user),
        })
            .then(response => response.json());
    }

    create(user) {
        return fetch('USERS_URL', {
            method: "POST",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(user),
        })
            .then(response => response.json());
    }

    getUser(userId) {
        return fetch('USERS_URL/'+ userId)
            .then(response => response.json());
    }
}