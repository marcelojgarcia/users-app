import './usersList.js';
import './editUser.js';
import './sessionsList.js';
import './login.js';
import AuthService from './authService.js';

import '@polymer/app-layout/app-layout.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';

import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@vaadin/vaadin-notification/vaadin-notification.js';
import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker.js';
import '@vaadin/vaadin-combo-box/vaadin-combo-box.js';
import '@vaadin/vaadin-form-layout/vaadin-form-layout.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-button/vaadin-button.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';

import { html, render } from 'lit-html';

class UsersApp extends HTMLElement {

    constructor() {
        super();
        console.log('constructor UsersPage');
        this.root = this.attachShadow({ mode: 'open' });
        this.authService = new AuthService();
        window.addEventListener('hashchange', e => this.onHashChange(e.target.location.hash));
    }

    connectedCallback() {
        console.log('connectedCallback UsersPage');
        render(this.template(),this.root);

        //On refresh reload same view
        console.log('Loading default...')
        this.onHashChange(window.location.hash);
    }

    template() {
            return html`
            <style>
                app-toolbar {
                    background-color: #4CAF50;
                    color: #fff;
                }
                paper-icon-button + [main-title] {
                    margin-left: 24px;
                } 
                .app-info {
                    margin: 80px 20px 0px 20px;
                    padding-bottom: 20px;
                    border-bottom: 1px solid #CCC;
                    text-align: center;
                }          
                .app-info .name {
                    font-weight: bold;
                    color: #4CAF50;
                }
                .app-info .description {
                    margin-top: 5px;
                    color: #999;
                }
                paper-item {
                    height: 54px;
                }
                paper-item > a {
                    width: 100%;
                    height: 100%;
                    line-height: 54px;
                    text-align: center;
                    text-decoration: none;
                    color: black;
                }    
            </style>
            <app-drawer-layout force-narrow>
                <app-drawer id="drawer" swipe-open slot="drawer">
                    <div class="app-info">
                        <div class="name">Users App</div>
                        <div class="description">Manage Registered Users</div>
                    </div>
                    <paper-listbox selected="[[_pageData.page]]" attr-for-selected="name" on-iron-activate="_drawerSelected">
                        <paper-item name="users">
                            <a href="#/users" name="users" drawer-toggle>Users</a>
                        </paper-item>
                        <paper-item name="sessions">
                            <a href="#/sessions" name="sessions" drawer-toggle>Sessions</a>
                        </paper-item>
                        <paper-item name="logout">
                            <a href="#/logout" name="logout" drawer-toggle>Logout</a>
                        </paper-item>
                    </paper-listbox>
                </app-drawer>
                <app-header-layout>
                </app-header-layout>
            </app-drawer-layout>`;
    }

    onHashChange(hash) {
        console.log('Hash changed', hash);

        const container = this.root.querySelector('app-header-layout')
        const route = hash.split('/')[1] || '';
        const subroute = hash.split('/')[2] || '';

        if (route === 'logout') {
            this.authService.logout();
        }

        //adicionar veiricação de validade do token nesta condição
        if (!window.localStorage.getItem('authToken') || !true) {
            render(this.loginComp(), container);
            return;
        }

        switch (route) {
            case 'users':
                if (!subroute) {
                    render(this.usersList(), container);
                } else {
                    render(this.editUser(subroute),container);
                }
                break;
            case 'sessions':
                render(this.sessionsList(), container);
                break;
            default:
                render(this.usersList(), container);
                break;
        }
    }

    loginComp() {
        return html`<user-login></user-login>`;
    }

    usersList() {
        return html`
        ${this.header('Users List')}
        <users-list></users-list>`;
    }

    sessionsList() {
        return html`
        ${this.header('Sessions List')}
        <sessions-list></sessions-list>`;
    }

    editUser(userid) {
        return html`        
        ${this.header('Edit User')}
        <edit-user data-userid="${userid}"></edit-user>`;
    }

    header(title){
        return html`
        <app-header slot="header">
            <app-toolbar>
                <paper-icon-button class="drawerToggler" icon="menu" drawer-toggle></paper-icon-button>
                <div main-title>${title}</div>
            </app-toolbar>
        </app-header>`;
    }
}

customElements.define('users-app', UsersApp);