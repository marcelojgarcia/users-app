
import AuthService from './authService.js';
import { html, render } from 'lit-html/lit-html.js';

class Login extends HTMLElement {

    constructor() {
        super();
        console.log('constructor Login');
        this.root = this.attachShadow({ mode: 'open' });
        this.authService = new AuthService();
    }

    connectedCallback() {
        console.log('connectedCallback Login');
        render(this.template(), this.root);
    }

    template() {

        const authenticate = {
            handleEvent: (e) => this.handleAuthentication(e),
            capture: true
        };

        return html`
        <style>
            :host{
                display: block;
                margin: 10px auto;
                padding: 0.5em;
                max-width: 700px;        
            }
            vaadin-button {
                background-color: var(--button-bk-color);
                color: var(--button-color);
            }
            .form {
                position: relative;
                z-index: 1;
                background: #FFFFFF;
                max-width: 460px;
                margin: 0 auto 0px;
                padding: 45px;
                text-align: center;
                box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
                }
                .form .message {
                margin: 15px 0 0;
                color: red;
                font-size: 16px;
                }
                .container {
                position: relative;  
                max-width: 700px;
                margin: 0px auto;
                padding: 0.5em;
                }
                .container:before, .container:after {
                content: "";
                display: block;
                clear: both;
                }
                .hidden{
                display: none;
                }
        </style>
        <div id='center' class="main center">
            <div class="container">
                <div class="login-page">
                    <div class="form">
                        <vaadin-form-layout id="loginForm" class="login-form">
                            <vaadin-text-field id="username" type="text" label="Username" error-message="Preenchimento obrigatório" required>
                            </vaadin-text-field>
                            <vaadin-password-field id="userpass" type="password" label="Password" error-message="Preenchimento obrigatório" required>
                            </vaadin-password-field>
                            <vaadin-button theme="large" id="login" type="submit" @click=${authenticate}>Submit</vaadin-button>
                            <p id="message" class="message hidden">Not authenticated!</p>
                        </vaadin-form-layout>
                    </div>
                </div>
            </div>
        </div>`;
    }

    handleAuthentication(e) {
        const username = this.root.getElementById('username');
        const userpass = this.root.getElementById('userpass');
        const message = this.root.querySelector('#message');

        username.validate();
        userpass.validate();

        if (username.invalid || userpass.invalid) {
            return;
        }

        this.authService.authenticate(username.value, userpass.value)
            .then(result => {

                if (result) {
                    window.location.hash = '#/users';
                } else {
                    message.classList.remove('hidden');
                }

            })
            .catch(reason => {
                console.error('Error:', reason);
                message.classList.remove('hidden');
            })
            .finally(() => {
                username.value = '';
                userpass.value = '';
            });
    }

}

customElements.define('user-login', Login);