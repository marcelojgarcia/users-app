import { html, render } from 'lit-html/lit-html.js';
import { UsersService } from './usersService.js';

class UsersList extends HTMLElement {

    constructor() {
        super();
        console.log('constructor UsersList');
        this.root = this.attachShadow({ mode: 'open' });
        this.usersService = new UsersService();
    }

    connectedCallback() {
        console.log('ConnectedCallback UsersList');
        render(this.template(), this.root);

        const buttonColumn = this.root.querySelector('vaadin-grid-column:nth-of-type(4)');
        buttonColumn.renderer = (root, column, rowData) => this.renderer(root, column, rowData);

        this.usersService.getUsers()
            .then(users => {
                let table = this.root.getElementById('usersTable');
                table.items = users;
            });
    }

    template() {
        return html`
        <style>
            :host{
                display: block;
                margin: 10px auto;
                padding: 0.5em;
                max-width: 700px;        
            }
            vaadin-grid{
                height: 75vh;
            }
            vaadin-button {
                background-color: var(--button-bk-color);
                color: var(--button-color);
            }
            .fab {
                position: fixed;
                width: 56px;
                right: 10%;
                bottom: 15px;
                margin-left: -28px;
              }
              .fab-button {
                position: absolute;
                bottom: 0;
                display: block;
                width: 56px;
                height: 56px;
                background-color: var(--button-bk-color);
                border-radius: 50%;
                box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
              }
              .fab-button__icon {
                display: inline-block;
                width: 56px;
                height: 56px;
                background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAABuElEQVRIidWXsU4bQRCGv/9kIWQhFBoXVpSamgZEkRcIz8QTJMoL8AgpkxqJCjqegYLCQiRIURQhMn+K3J42lztza3Oy8rvweWZv//l3Z2bXMmYTqDbCCkyKRpvK9gyYSaoAbP8EblTpx3jEsC/pAzDLbE+SvmDeI4aTu+Rjn9p2RDihfn6IiHnJXEV7bHsPQFJjq59309IPxcaS6/8jto29eg8ozeoG+T6nAIT6hr8ccU6aB1FM7HAl6RUw7ZtcaKftk5T8c8e/QdSrEZK+/VXnqa4i4sD2eUT88jPI63ggHuoeMO2q4yPbx0C1buJ0YBd4B7xJhmapJW0BW0NmKd3TWsRE0nayLS2nXHn7u2Pipb52sO2sjjyYjtbYqXaIrw4iUsnlim+B+3akfUqSvW9ch28haZF+54o/80fxoe1JellSHvVb4KCtpiY4A77n7yRySV8lfbK9aJpMyVEWER+XlNjr0Y7FlsJO31CUnscrkaxNnPatL6DRiN1zIKyyAmtdBLoazFjETzkpNGpj1OSSdGX7Liey/QhcQMHVFlDRXxgzBU6AeWZ9BC5tX6tSjEP8gtjYLfM3WA7GhFCw/pYAAAAASUVORK5CYII=") center no-repeat;
              }      
        </style>            
        <vaadin-grid id="usersTable" theme="row-stripes">
            <vaadin-grid-column path="username" header="Username"></vaadin-grid-column>
                <vaadin-grid-column path="name" header="Name"></vaadin-grid-column>
                <vaadin-grid-column path="email" header="E-mail" width="200px"></vaadin-grid-column>
                <vaadin-grid-column width="14em" flex-grow="0">
            </vaadin-grid-column>
        </vaadin-grid>
        <div class="fab">
            <a href="#/users/0" class="fab-button">
                <i class="fab-button__icon"></i>
            </a>
        </div>
        `;
    }

    renderer(root, column, rowData) {

        let wrapper = root.firstElementChild;
        if (!wrapper) {
            render(this.editButton(rowData),root);
            wrapper = root.firstElementChild;
        }

        // We reuse rendered content, but maintain a property with the index for actions
        wrapper.idx = rowData.index;
    };

    editButton(rowData){
        return html`
        <div style="text-align: right">
            <vaadin-button aria-label="Edit" theme="icon" focus-target @click=${this.editHandler(rowData)}>
                <iron-icon icon="vaadin:edit"></iron-icon>
            </vaadin-button>
        </div>
        `;
    }

    editHandler(rowData){
        return {
            handleEvent: (e)=>window.location.href = '#/users/' + rowData.item.id,
            capture: true
        }
    }
}

customElements.define('users-list', UsersList);