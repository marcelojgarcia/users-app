import { html, render } from 'lit-html/lit-html.js';

class SessionsList extends HTMLElement {

    constructor() {
        super();
        console.log('constructor SessionsList');
        this.root = this.attachShadow({ mode: 'open' });
    }

    connectedCallback() {
        console.log('connectedCallback SessionsList');
        render(this.template(), this.root);

        const buttonColumn = this.root.querySelector('vaadin-grid-column:nth-of-type(4)');
        buttonColumn.renderer = (root, column, rowData) => this.renderer(root, column, rowData);

        fetch('SESSIONS_URL')
            .then(response => response.json())
            .then(users => {
                const table = this.root.getElementById('sessionsTable');
                table.items = users;
            });
    }

    template() {
        return html`
        <style>
            :host{
                display: block;
                margin: 10px auto;
                padding: 0.5em;
                max-width: 700px;      
                    max-width: 700px;      
                max-width: 700px;      
            }
            vaadin-button {
                background-color: var(--button-bk-color);
                color: var(--button-color);
            }
        </style>
        <vaadin-grid id="sessionsTable" theme="row-stripes">
            <vaadin-grid-column path="id" header="#"></vaadin-grid-column>
            <vaadin-grid-column path="username" header="Username"></vaadin-grid-column>
            <vaadin-grid-column path="timestamp" header="Timestamp" width="200px"></vaadin-grid-column>
            <vaadin-grid-column width="14em" flex-grow="0"></vaadin-grid-column>
        </vaadin-grid>`;
    }

    deleteButton() {
        return html`
        <div style="text-align: right">
            <vaadin-button aria-label="Delete" theme="icon" focus-target @click=${this.deleteHandler()}>Delete</vaadin-button>
        </div>`;
    }

    deleteHandler(){
        return {
            handleEvent: (e) =>{
                console.log('Session deleted!');
                console.log(e);
            },
            capture: true
        };
    }
    
    renderer(root, column, rowData) {

        let wrapper = root.firstElementChild;
        if (!wrapper) {
            render(this.deleteButton(), root);
            wrapper = root.firstElementChild;
        }

        // We reuse rendered content, but maintain a property with the index for actions
        wrapper.idx = rowData.index;
    }
}

customElements.define('sessions-list', SessionsList);