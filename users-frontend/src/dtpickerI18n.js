export default class DatePickerI18n {

    constructor() {
    }

    addI18n(datepicker) {

        Sugar.Date.setLocale('pt');
        datepicker.i18n = {
            week: 'semana',
            calendar: 'calendário',
            clear: 'limpar',
            today: 'hoje',
            cancel: 'cancelar',
            firstDayOfWeek: 2,
            monthNames:
                'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
            weekdays: 'segunda_terça_quarta_quinta_sexta_sábado_domingo'.split('_'),
            weekdaysShort: 'se_te_qa_qi_se_sa_do'.split('_'),
            formatDate: function (date) {
                return Sugar.Date.format(Sugar.Date.create(date), '{short}');
            },
            formatTitle: function (monthName, fullYear) {
                return monthName + ' ' + fullYear;
            },
            parseDate: function (dateString) {
                const date = Sugar.Date.create(dateString);
                return {
                    day: date.getDate(),
                    month: date.getMonth(),
                    year: date.getFullYear()
                };
            }
        };
    }

}