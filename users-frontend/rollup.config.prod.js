import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import { terser } from "rollup-plugin-terser";

const MAIN_BUNDLE = {
    input: 'src/main.js',
    output: {
        file: 'build/bundle.min.js',
        format: 'esm'
    },
    plugins: [
        resolve({ jsnext: true }),
        replace({
            exclude: 'node_modules/**',
            USERS_URL: process.env.USERS_URL || 'json/users.json',
            SESSIONS_URL: process.env.SESSIONS_URL || 'json/sessions.json'
        }),
        terser()
    ]
}

export default [
    Object.assign({}, MAIN_BUNDLE)
]
