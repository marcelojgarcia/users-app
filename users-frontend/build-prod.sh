#!/bin/sh
cd "${0%/*}"

#Backend URLs
export SESSIONS_URL='http://localhost:8080/resources/sessions'
export USERS_URL='http://localhost:8080/resources/users'

rm -rf build && mkdir build && cp src/index.html src/style.css ./build
cp lib/sugar-custom.min.js ./build/sugar.js
rollup --config rollup.config.prod.js