#!/bin/sh

#Backend URLs
export SESSIONS_URL='http://localhost:8080/users-backend/resources/sessions'
export USERS_URL='http://localhost:8080/users-backend/resources/users'

rm -rf build && mkdir build && cp src/index.html src/style.css ./build
cp lib/sugar-custom.js ./build/sugar.js
rollup --config rollup.config.dev.js --watch
