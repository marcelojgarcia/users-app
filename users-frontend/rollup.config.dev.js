import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';

const BUNDLE_1= {
    input: 'src/main.js',
    output:{
        file: 'build/bundle.min.js',
        format: 'esm',
        sourceMap: 'inline'
    },    
    plugins: [
        resolve({ jsnext: true }),
        replace({
            exclude: 'node_modules/**',
            USERS_URL: process.env.USERS_URL ||'json/users.json',
            SESSIONS_URL: process.env.SESSIONS_URL || 'json/sessions.json'
        })
      ]
}

export default [
    Object.assign({},BUNDLE_1)
]
