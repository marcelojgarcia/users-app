#!/bin/sh
cd "${0%/*}"
mvn clean package -Pcreate-docker
docker build -t users-backend .
