package br.com.xtremeti.users.backend.business.appuser.entity;

import java.text.ParseException;
import java.util.Date;
import javax.json.JsonObject;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsNull.nullValue;
import org.junit.Test;
import static org.junit.Assert.*;

public class AppUserTest {

    @Test
    public void testFromJSON() throws ParseException {

        String json = "{"
                + "\"username\":\"testCriarUsuario\","
                + "\"name\":\"Test User\","
                + "\"email\":\"testuser@test.com\","
                + "\"birthDate\":\"2019-01-01\","
                + "\"profile\":\"administrador\""
                + "}";

        AppUser appuser = AppUser.fromJSON(json);

        assertThat(appuser.getUsername(), is("testCriarUsuario"));
        assertThat(appuser.getName(), is("Test User"));
        assertThat(appuser.getEmail(), is("testuser@test.com"));
        assertThat(appuser.getBirthDate(), not(nullValue()));
        assertThat(appuser.getProfile(), is("administrador"));
    }

    @Test
    public void testToJSON() throws ParseException {

        AppUser appuser = new AppUser("testCriarUsuario", "Test User", "testuser@test.com",
                new Date(), "administrador");

        JsonObject json = appuser.toJson();

        assertThat(json.getString("username"), is("testCriarUsuario"));
        assertThat(json.getString("name"), is("Test User"));
        assertThat(json.getString("email"), is("testuser@test.com"));
        assertThat(json.getString("birthDate"), not(nullValue()));
        assertThat(json.getString("profile"), is("administrador"));
    }
}
