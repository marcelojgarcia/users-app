package br.com.xtremeti.users.backend.business.appuser.boundary;

import br.com.xtremeti.users.backend.business.appuser.entity.AppUser;
import java.util.Date;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class AppUsersIT {

    private AppUsers cut;
    private EntityManagerFactory emf;
    private EntityTransaction et;

    @Before
    public void before() {

        emf = Persistence.createEntityManagerFactory("it");
        cut = new AppUsers();
        cut.em = emf.createEntityManager();
        et = cut.em.getTransaction();

    }

    @After
    public void after() {
        emf.close();
    }

    @Test
    public void testIsUsernameTaken() throws Exception {

        AppUser appuser = new AppUser("username", "name", "email", new Date(), "");
        et.begin();
        cut.create(appuser);
        et.commit();

        assertThat(cut.isUsernameTaken(appuser.getUsername()), is(true));
        assertThat(cut.isUsernameTaken("nottaken"), is(false));
    }

}
