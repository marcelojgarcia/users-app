package br.com.xtremeti.users.backend.business.appuser.boundary;

/**
 * Entity not found for id provided.
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
public class NotFoundException extends Exception {

    private final long id;
    
    public NotFoundException(long id) {
        super();
        this.id = id;
    }
    
    public long getId(){
        return this.id;
    }
}
