package br.com.xtremeti.users.backend.business.session.entity;

import java.util.Date;

/**
 * Sessão ativa da aplicação.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
public class Session {

    private long id;
    private String username;
    private Date timestamp;

    public Session(String username, Date timestamp) {
        this.username = username;
        this.timestamp = timestamp;
    }
    
    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}
