package br.com.xtremeti.users.backend.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configuração de recursos REST.
 * @author Marcelo Garcia <marceljgarcia@xtremeti.com.br>
 */
@ApplicationPath("resources")
public class ResourcesConfig extends Application{
    
}
