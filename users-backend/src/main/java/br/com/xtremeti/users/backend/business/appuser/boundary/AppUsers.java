package br.com.xtremeti.users.backend.business.appuser.boundary;

import br.com.xtremeti.users.backend.business.appuser.entity.AppUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * AppUser management.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
@Stateless
public class AppUsers {

    @PersistenceContext
    EntityManager em;

    /**
     * Lists all registered AppUser's.
     *
     * @return Lista de todos os usuários registrados
     */
    public List<AppUser> getUsers() {

        return this.em.createNamedQuery(AppUser.ALL, AppUser.class)
                .getResultList();
    }

    /**
     * Find AppUser by id.
     *
     * @param id The id
     * @return AppUser
     */
    public AppUser getById(long id) {

        return this.em.find(AppUser.class, id);
    }

    /**
     * Creates new AppUser.
     *
     * @param appuser The new AppUser
     * @return Registered AppUser
     * @throws
     * br.com.xtremeti.users.backend.business.appuser.boundary.InvalidAppUserException
     */
    public AppUser create(AppUser appuser) throws InvalidAppUserException {

        if (!appuser.isValid()) {
            throw new InvalidAppUserException(appuser);
        }

        if (this.isUsernameTaken(appuser.getUsername())) {
            throw new InvalidAppUserException(appuser, "The username is already taken");
        }

        this.em.persist(appuser);

        return appuser;
    }

    /**
     * Updates AppUser.
     *
     * @param id The AppUser id
     * @param appuser The AppUser to update
     * @return Updated AppUser
     * @throws
     * br.com.xtremeti.users.backend.business.appuser.boundary.InvalidAppUserException
     * @throws
     * br.com.xtremeti.users.backend.business.appuser.boundary.NotFoundException
     */
    public AppUser update(long id, AppUser appuser)
            throws InvalidAppUserException, NotFoundException {

        if (!appuser.isValid()) {
            throw new InvalidAppUserException(appuser);
        }

        AppUser registered = this.getById(id);
        if (registered == null) {
            throw new NotFoundException(id);
        }

        registered.setBirthDate(appuser.getBirthDate());
        registered.setEmail(appuser.getEmail());
        registered.setName(appuser.getName());
        registered.setProfile(appuser.getProfile());

        return registered;
    }

    /**
     * Checks if username is taken
     *
     * @param username Username to check
     * @return true if username is taken
     */
    boolean isUsernameTaken(String username) {

        return this.em.createNamedQuery(AppUser.FIND_BY_USERNAME, Boolean.class)
                .setParameter("username", username)
                .getSingleResult();
    }

}
