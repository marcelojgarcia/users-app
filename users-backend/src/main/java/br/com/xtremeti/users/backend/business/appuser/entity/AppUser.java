package br.com.xtremeti.users.backend.business.appuser.entity;

import java.io.Serializable;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AppUser.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
@Entity()
@NamedQueries({
    @NamedQuery(name = AppUser.ALL, query = "select u from AppUser u")
    ,
        @NamedQuery(name = AppUser.FIND_BY_USERNAME, query = "select case when (count(u) > 0)  then true else false end from AppUser u where u.username = :username")
})
public class AppUser implements Serializable {

    public static final String ALL = "AppUser.all";
    public static final String FIND_BY_USERNAME = "AppUser.findByUsername";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false, name = "firstname")
    private String name;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Column(nullable = false)
    private String profile;

    public AppUser() {
    }

    public AppUser(String username, String name, String email, Date dtNascimento,
            String perfil) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.birthDate = dtNascimento;
        this.profile = perfil;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    /**
     * Generates JSON from this AppUser.
     *
     * @return JSON Object
     */
    public JsonObject toJson() {

        String formattedDtNascimento = SDF.format(this.getBirthDate());

        JsonObjectBuilder builder = Json.createObjectBuilder();

        return builder.add("id", this.getId())
                .add("username", this.getUsername())
                .add("name", this.getName())
                .add("email", this.getEmail())
                .add("birthDate", formattedDtNascimento)
                .add("profile", this.getProfile())
                .build();
    }

    /**
     * Create AppUser from JSON
     *
     * @param json The JSON
     * @return AppUser
     * @throws java.text.ParseException
     */
    public static AppUser fromJSON(String json)
            throws ParseException {

        AppUser appuser = new AppUser();

        if (json == null || "".equals(json)) {
            return appuser;
        }

        try (JsonReader reader = Json.createReader(new StringReader(json))) {

            JsonObject appuserJSON = reader.readObject();

            appuser.setUsername(appuserJSON.getString("username", null));
            appuser.setEmail(appuserJSON.getString("email", null));
            appuser.setName(appuserJSON.getString("name", null));
            appuser.setProfile(appuserJSON.getString("profile", null));

            String birthDate = appuserJSON.getString("birthDate", null);
            if (birthDate != null) {
                appuser.setBirthDate(SDF.parse(birthDate));
            }

        }

        return appuser;
    }

    /**
     * Checks if AppUser is valid.
     *
     * @return true if AppUser is valid
     */
    public boolean isValid() {
        return this.username != null
                && this.name != null
                && this.email != null
                && this.profile != null
                && this.birthDate != null;
    }

}
