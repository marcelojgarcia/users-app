package br.com.xtremeti.users.backend.business.appuser.boundary;

import br.com.xtremeti.users.backend.business.appuser.entity.AppUser;

/**
 * Invalid AppUser provided.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
public class InvalidAppUserException extends Exception {

    private final AppUser appuser;

    public InvalidAppUserException(AppUser appuser) {
        super("Invalid JSON");
        this.appuser = appuser;
    }

    public InvalidAppUserException(AppUser appuser, String message) {
        super(message);
        this.appuser = appuser;
    }

    public AppUser getAppuser() {
        return appuser;
    }

}
