package br.com.xtremeti.users.backend.business.session.boundary;

import br.com.xtremeti.users.backend.business.session.entity.Session;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;

/**
 * Recursos REST para sessões.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
@Stateless
@Path("sessions")
public class SessionsResources {

    @EJB
    Sessions sessionsService;

    @GET
    @Produces("application/json")
    @Counted(monotonic = true)
    public Response getSessions() {

        List<Session> sessions = sessionsService.getSessions();

        if (sessions.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(sessions, MediaType.APPLICATION_JSON).build();
    }
}
