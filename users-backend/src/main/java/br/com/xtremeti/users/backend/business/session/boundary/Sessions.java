package br.com.xtremeti.users.backend.business.session.boundary;

import br.com.xtremeti.users.backend.business.session.entity.Session;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Gerenciamento de sessões.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
@Stateless
public class Sessions {

    public List<Session> getSessions() {

        List<Session> sessions = new ArrayList<>();
        sessions.add(new Session("username1", new Date()));
        sessions.add(new Session("username2", new Date()));

        return sessions;
    }
}
