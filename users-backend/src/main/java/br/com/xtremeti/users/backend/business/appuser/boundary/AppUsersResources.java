package br.com.xtremeti.users.backend.business.appuser.boundary;

import br.com.xtremeti.users.backend.business.appuser.entity.AppUser;
import java.text.ParseException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.metrics.annotation.Counted;

/**
 * Recursos REST para Usuários.
 *
 * @author Marcelo Garcia <marcelojgarcia@xtremeti.com.br>
 */
@Stateless
@Path("users")
public class AppUsersResources {

    @EJB
    AppUsers usersService;

    @GET
    @Produces("application/json")
    @Counted(monotonic = true)
    public Response getUsers() {

        List<AppUser> users = usersService.getUsers();

        if (users.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JsonArrayBuilder list = Json.createArrayBuilder();
        users.stream().map(AppUser::toJson).forEach(list::add);

        return Response.ok(list.build(), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces("application/json")
    @Path("{id}")
    public Response gerUser(@PathParam("id") long id) {

        AppUser appuser = usersService.getById(id);

        if (appuser == null) {
            return Response.status(404).build();
        }

        return Response.ok(appuser.toJson()).build();
    }

    @POST
    @Produces("application/json")
    public Response create(String json) {

        try {

            AppUser appuser = AppUser.fromJSON(json);
            AppUser created = usersService.create(appuser);

            return Response.ok(created).build();

        } catch (ParseException ex) {
            return fromException(ex, 422);
        } catch (InvalidAppUserException ex) {
            return fromException(ex, 400);
        }

    }

    @PUT
    @Produces("application/json")
    @Path("{id}")
    public Response update(@PathParam("id") long id, String json) {

        try {

            AppUser appuser = AppUser.fromJSON(json);
            AppUser updated = usersService.update(id, appuser);

            return Response.ok(updated).build();

        } catch (ParseException ex) {
            return fromException(ex, 422);
        } catch (InvalidAppUserException ex) {
            return fromException(ex, 400);
        } catch (NotFoundException ex) {
            return Response.status(404).build();
        }

    }

    private Response fromException(Exception ex, int statusCode) {
        return Response.status(statusCode)
                .entity("{\"error\":\"" + ex.getMessage().replaceAll("\"", "") + "\"}")
                .build();

    }
}
