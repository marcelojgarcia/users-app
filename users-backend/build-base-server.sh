#!/bin/sh
cd "${0%/*}"
docker build --build-arg USERNAME=postgres --build-arg PASSWORD=postgres --file Dockerfile.wildfly.postgres -t wildfly-postgres .
